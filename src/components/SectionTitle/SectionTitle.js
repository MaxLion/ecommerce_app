import React from 'react';

import styles from './sectionTitle.module.scss'

const SectionTitle = ({ children }) => (<span className={styles.title}>{children}</span>);

export default SectionTitle;