import React from 'react';
import styles from './directoryItem.module.scss';
import { withRouter } from 'react-router-dom';
import DirectoryOverviewItem from '../DirectoryOverviewItem/DirectoryOverviewItem'
import { ReactComponent as Logo } from '../../assets/images/logo.svg';
import SubmenuBlock from '../SubmenuBlock/SubmenuBlock';

const DirectoryItem = ({ src, name, url, match, history, items, criterias }) => {

    return (
        <div className={styles.directoryItem} >
            <h1 className={styles.categoryName} onClick={() => history.push(`${match.url}${url}`)}><span>{name}</span></h1>
            <div className={styles.content}>
                <div className={styles.overviewItems}>
                    {items.filter((item, index) => (index < 3)).map(
                        (item) => <DirectoryOverviewItem key={item.id} src={item.imageUrl} exploreUrl={`${match.url}${url}/${item.id}`} />
                    )}
                </div>
                <div className={styles.subMenu}>
                    <div className={styles.LogoContainer}>
                        <Logo />
                    </div>
                    {
                        (criterias) ?
                            criterias.map(
                                (criteria) => <SubmenuBlock key={criteria.id} query={criteria.query} name={criteria.name} criterias={criteria.criterias} base={`${match.url}${url}`} />
                            ) :
                            null
                    }
                </div>
            </div>
        </div>
    )
};

export default withRouter(DirectoryItem);
