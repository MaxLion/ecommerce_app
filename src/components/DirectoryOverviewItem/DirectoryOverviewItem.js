import React from 'react';
import { withRouter } from 'react-router-dom';
import styles from './directoryOverviewItem.module.scss';

const DirectoryOverviewItem = ({ src, exploreUrl, history }) => (
    <div className={styles.overviewItem}>
        <div style={{ backgroundImage: `url(${src})` }} className={styles.BackgroundImage} />
        <div className={styles.detail} onClick={() => history.push(`${exploreUrl}`)}>
            <p className={styles.Subtitle}>Explore Now</p>
        </div>
    </div>
)

export default withRouter(DirectoryOverviewItem);