import React from 'react'
import styles from './collectionItem.module.scss';
import data from '../../data.js';
import ImageGallery from '../ImageGallery/ImageGallery';
import ItemAttributes from '../ItemAttributes/ItemAttributes';
import SectionTitle from '../SectionTitle/SectionTitle';
import { Link } from 'react-router-dom'

const CollectionItem = ({ match }) => {
    const itemId = match.params.itemId;
    const collectionId = match.params.collectionId;
    const item = data[collectionId].items.find(element => element.id === parseInt(itemId))

    return (
        <div>
            <Link to={`${match.url.replace(`/${item.id}`, '')}`}>
                <SectionTitle>{`${match.params.collectionId}`}</SectionTitle>
            </Link>
            <div className={styles.collectionItem}>
                <ImageGallery className={styles.gallery} images={item.gallery} />
                <ItemAttributes
                    itemTag={(item.tag) ? item.tag : null}
                    itemName={item.name}
                    price={item.price}
                    rate={item.rate}
                    sizes={item.sizes}
                    colors={item.colors} />
            </div>
        </div>
    );
}

export default CollectionItem;