import React from 'react';
import styles from './colorPicker.module.scss';

class ColorPicker extends React.Component {

    state = {
        selected: ''
    }

    selectedColorHandler = (color) => {
        this.setState({ selected: color })
    }

    render() {
        const { selected } = this.state;
        return (
            <div className={styles.colorPicker}>
                {
                    this.props.colors.map(
                        (color, index) => <span key={index} onClick={(e) => this.selectedColorHandler(color)} style={{ backgroundColor: color }} className={(selected === color) ? [styles.box, styles.selected].join(' ') : styles.box}></span>
                    )
                }
            </div>
        )
    }
}

export default ColorPicker;