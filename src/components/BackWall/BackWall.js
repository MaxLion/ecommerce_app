import React from 'react';
import styles from './backwall.module.scss';

const BackWall = ({ position, blur }) => {
    return (
        <div className={styles.containerBackground}>
            <div className={[styles.circle, styles.blury].join(' ')} style={{ top: '-20em', right: '-10em', backgroundColor: '#ffbbcc', filter: `blur(${blur})` }}></div>
            <div className={[styles.circle, styles.blury].join(' ')} style={{ bottom: '-20em', left: '-10em', backgroundColor: '#ffcccc', filter: `blur(${blur})` }}></div>
        </div>
    );
}

export default BackWall;