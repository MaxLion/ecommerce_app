import React from 'react'
import style from './customSelector.module.scss';

const CustomSelector = ({ children, inverted, ...otherProps }) => (
    <div className={`${style.customSelector}`}>
        <label>
            <select>
                {children}
            </select>
        </label>
    </div>
    // <button className={`${isGoogleSignIn ? style.GoogleSignIn : ''}${inverted ? style.Inverted : ''} ${style.CustomButton}`} {...otherProps}>
    //     {children}
    // </button>
);

export default CustomSelector;