import React from 'react';
import styles from './rateStars.module.scss'

const RateStars = ({ ranking }) => {
    let rate = (Array(5).fill(0)).fill(1, 0, ranking)

    return (

        <div className={styles.ItemAttributes}>
            {rate.map((star, index) => <span key={index} className={(star) ? ["fa fa-star", styles.colored].join(' ') : "fa fa-star"}></span>)}
        </div>
    )
}

export default RateStars;