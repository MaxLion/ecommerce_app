import React from 'react'
import styles from './collection.module.scss'
import CollectionItemOverview from '../CollectionItemOverview/CollectionItemOverview'
import SectionTitle from '../SectionTitle/SectionTitle';
import data from '../../data.js';

const Collection = ({ match }) => {
    const collectionId = match.params.collectionId;
    const collectionIems = data[collectionId].items;
    console.log(match);
    return (
        <div>
            <SectionTitle>{collectionId}</SectionTitle>
            <div className={styles.collection}>
                {
                    collectionIems.map(
                        (item) => (
                            <CollectionItemOverview
                                key={item.id}
                                src={item.imageUrl}
                                url="shop/dresses"
                                name={item.name}
                                itemId={item.id}
                                price={item.price}
                            />
                        )
                    )
                }
            </div>
        </div>
    )
};

export default Collection;