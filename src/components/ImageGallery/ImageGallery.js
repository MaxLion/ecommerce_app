import React from 'react';
import styles from './imageGallery.module.scss'
import ThumbnailsGallery from '../ThumbnailsGallery/ThumbnailsGallery';

class ImageGallery extends React.Component {

    state = {
        currentImage: '',

    }

    selectedImageHandler = (childData) => {
        this.setState({ currentImage: childData })
    }

    mouseOverHandler = ({ target }) => {
        target.style.transform = `scale(2.5)`
    }

    mouseOutHandler = ({ target }) => {
        target.style.transform = `scale(1)`
    }

    mouseMoveHandler = ({ target, pageX, pageY }) => {
        let offsetTop = target.offsetTop;
        let offsetLeft = target.offsetLeft;
        let width = target.width;
        let height = target.height;
        let transformed = ((pageX - offsetLeft) / width) * 100 + '% ' + ((pageY - offsetTop) / height) * 100 + '%'

        target.style.transformOrigin = `${transformed}`
    }

    render() {
        const { currentImage } = this.state;
        return (
            <div className={styles.galleryContainer}>
                <ThumbnailsGallery images={this.props.images} selectedImage={this.selectedImageHandler} />
                <div className={styles.image}>
                    <img
                        onMouseMove={(e) => this.mouseMoveHandler(e)}
                        onMouseOut={(e) => this.mouseOutHandler(e)}
                        onMouseOver={(e) => this.mouseOverHandler(e)}
                        src={currentImage}
                        alt="thumbnail"
                    />
                </div>
            </div>
        )
    }
}

export default ImageGallery;