import React from 'react';
import styles from './itemAttributes.module.scss'
import RateStars from '../RateStars/RateStars';
import ColorPicker from '../ColorPicker/ColorPicker';
import CustomSelector from '../CustomSelector/CustomSelector';
import CustomButton from '../CustomButton/CustomButton';

const ItemAttributes = ({ itemTag, itemName, rate, price, colors, sizes, qty }) => (
    <div className={styles.ItemAttributes}>
        <h4 className={styles.tag}>{itemTag}</h4>
        <h1 className={styles.name}>{itemName}</h1>
        <RateStars ranking={rate} />
        <h1>${price.toFixed(2)}</h1>
        <h3>Available Colors</h3>
        <ColorPicker colors={colors} />
        <CustomSelector>
            <option value="">Select size</option>
            {sizes.map((size, index) => <option key={index} value={`${size}`}>{size}</option>)}
        </CustomSelector>
        <CustomButton>Add to cart</CustomButton>
    </div>
);

export default ItemAttributes;