import React from "react";
import style from "./header.module.scss";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { ReactComponent as Heart } from "../../assets/images/heart.svg";
import { ReactComponent as ShopBag } from "../../assets/images/shopping-bag.svg";
import { ReactComponent as User } from "../../assets/images/user.svg";
import { ReactComponent as Logo } from "../../assets/images/logo.svg";

const Header = ({ currentUser }) => (
  <div className={style.Header}>
    <Link className={style.LogoContainer} to="/">
      <Logo />
    </Link>
    <div className={style.Options}>
      <Link className={style.Option} to="/likes">
        <Heart className={style.Icon} />
      </Link>
      <Link className={style.Option} to="/shop">
        <ShopBag className={style.Icon} />
      </Link>
      {currentUser ? (
        <Link className={style.Option} to="/user">
          <User className={style.Icon} />
        </Link>
      ) : (
        <Link className={style.Option} to="/signin">
          Sign In
        </Link>
      )}
    </div>
  </div>
);

const mapStateToProps = (state) => ({
  currentUser: state.user.currentUser,
});

export default connect(mapStateToProps)(Header);
