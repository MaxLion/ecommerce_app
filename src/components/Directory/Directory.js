import React from 'react'
import styles from './directory.module.scss';
import DirectoryItem from '../DirectoryItem/DirectoryItem'
import SectionTitle from '../SectionTitle/SectionTitle';
import data from '../../data';

const Directory = () => (
    <div>
        <SectionTitle>CATEGORIES</SectionTitle>
        <div className={styles.directory}>
            {Object.keys(data).map((section) => {
                let sectionData = data[section];
                return <DirectoryItem key={sectionData.id} items={sectionData.items} criterias={sectionData.shopCriteria} name={sectionData.title} url={`shop/${sectionData.routeName}`} />
            })}
        </div>
    </div>
);

export default Directory;