import React from 'react';
import styles from './collectionItemOverview.module.scss';
import { ReactComponent as Heart } from '../../assets/images/heart.svg';
import { withRouter } from 'react-router-dom';

const CollectionItemOverview = ({ src, name, url, match, history, itemId, price }) => {

    console.log(`${match.url}/${itemId}`)
    return (
        <div className={styles.collection} onClick={() => history.push(`${match.url}/${itemId}`)} >
            <div style={{ backgroundImage: `url(${src})` }} className={styles.image} />
            <div className={styles.detail}>
                <p className={styles.name}>{name}</p>
                <div className={styles.bottom}>
                    <span className={styles.price}>{`$${price.toFixed(2)}`}</span>
                    <div className={styles.controls}>
                        <div className={styles.Option}>
                            <Heart className={styles.Icon} />
                        </div>
                        <div className={styles.Option}>
                            <div className={[styles.plusButton, styles.Icon].join(" ")}></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default withRouter(CollectionItemOverview);