import React, { Component } from 'react';
import styles from './thumbnailsGallery.module.scss'
import CustomButton from '../CustomButton/CustomButton';
class ThumbnailsGallery extends Component {
    state = {
        currentPage: 1,
        imagesPerPage: 2,
        images: [],
        collected: [],
        start: 1,
        selected: 0
    }

    componentDidMount() {
        this.setState({ images: this.props.images, collected: this.props.images });
        this.props.selectedImage(this.props.images[this.state.selected])
    }

    numPages = () => {
        return Math.ceil(this.state.images.length / this.state.imagesPerPage);
    }

    changePage = (page) => {
        return this.state.images.slice((page - 1) * this.state.imagesPerPage, (page * this.state.imagesPerPage));
    }

    previewPageHandler = () => {
        if (this.state.currentPage > 1) {
            this.setState((prevState) => ({ currentPage: prevState.currentPage - 1, selected: 0 }))
        }
    }

    nextPageHandler = (e) => {
        e.preventDefault();
        if (this.state.currentPage < this.numPages()) {
            this.setState((prevState) => ({ currentPage: prevState.currentPage + 1, selected: 0 }))
        }
    }

    onMouseEnterHandler = (url, index, element) => {
        this.setState({ selected: index });
        this.props.selectedImage(url)
    }

    render() {
        let { currentPage, selected } = this.state;

        console.log(this.state);

        return (
            <div className={styles.thumbnailsGallery}>
                <CustomButton onClick={() => this.previewPageHandler()}> <div className={styles.arrow}>&#10094;</div> </CustomButton>

                <div className={styles.thumbs}>
                    {
                        this.changePage(currentPage).map(
                            (url, index) =>
                                <img
                                    className={(selected === index) ? [styles.hovered].join(' ') : null}
                                    onMouseEnter={(e) => this.onMouseEnterHandler(url, index, e)}
                                    key={index}
                                    src={url}
                                    alt="thumbnail" />
                        )
                    }
                </div>

                <CustomButton onClick={(e) => this.nextPageHandler(e)}> <div className={styles.arrow}>&#10095;</div> </CustomButton>
            </div>
        )
    }
}

export default ThumbnailsGallery;