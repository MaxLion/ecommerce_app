import React from 'react'
import style from './customButton.module.scss';

const CustomButton = ({ children, isGoogleSignIn, inverted, ...otherProps }) => (
    <button className={`${isGoogleSignIn ? style.GoogleSignIn : ''}${inverted ? style.Inverted : ''} ${style.CustomButton}`} {...otherProps}>
        {children}
    </button>
);

export default CustomButton;