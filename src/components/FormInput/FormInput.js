import React from "react";
import style from "./forminput.module.scss";

const FormInput = ({ handleChange, label, ...otherProps }) => (
  <div className={style.Group}>
    <input
      className={style.FormInput}
      onChange={handleChange}
      {...otherProps}
    />
    {label ? (
      <label
        className={`${otherProps.value.length ? style.Shrink : ""} ${
          style.FormInputLabel
        }`}
      >
        {label}
      </label>
    ) : null}
  </div>
);

export default FormInput;
