import React from 'react';
import styles from './submenuBlock.module.scss';
import { Link } from 'react-router-dom';

const SubmenuBlock = ({ name, criterias, base, query }) => {
    // console.log(`${base}/${}`)
    return (
        <div className={styles.block}>
            <h3>{name}</h3>
            <ul>
                {criterias.map((criteria, index) => (
                    <Link key={index} to={`${base}?${query}=${criteria.route}`}>
                        <li>
                            {criteria.label}.
                        </li>
                    </Link>
                ))}
            </ul>
        </div>
    );
}

export default SubmenuBlock;