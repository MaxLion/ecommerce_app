const SHOP_DATA = {
    dresses: {
        id: 1,
        title: 'Dresses',
        routeName: 'dresses',
        shopCriteria: [
            {
                id: 1,
                name: 'DRESSES',
                query: 'q',
                criterias: [
                    { label: 'Shop All Dresses', route: '' },
                    { label: 'New Arrivals', route: 'arrivals' },
                    { label: 'Best Sellers', route: 'best' },
                    { label: 'Sale', route: 'sale' },
                ]
            },
            {
                id: 2,
                name: 'SHOP BY STYLE',
                query: 'style',
                criterias: [
                    { label: 'Mini Dresses', route: 'mini' },
                    { label: 'Midi Dresses', route: 'midi' },
                    { label: 'Maxi Dresses', route: 'maxi' },

                    { label: 'Sweater Dresses', route: 'sweater' },
                    { label: 'Sequin Dresses', route: 'sequin' },
                    { label: 'Satin Dresses', route: 'satin' },
                ]
            },
            {
                id: 3,
                name: 'SHOP BY OCCASION',
                query: 'occasion',
                criterias: [
                    { label: 'Formal Dresses', route: 'formal' },
                    { label: 'Girls Night Out Dresses', route: 'night_out' },
                    { label: 'Dresses Under $20', route: 'under_20' },
                    { label: 'Luxe Dresses', route: 'luxe' },
                    { label: 'Wear To Work Dresses', route: 'work' },
                ]
            },
            {
                id: 4,
                name: 'SHOP BY COLOR',
                query: 'color',
                criterias: [
                    { label: 'Black Dresses', route: 'black' },
                    { label: 'White Dresses', route: 'white' },
                    { label: 'Red Dresses', route: 'red' },
                    { label: 'Pink Dresses', route: 'pink' },
                    { label: 'Burgundy Dresses', route: 'burgundy' },
                ]
            },
        ],
        items: [
            {
                id: 1,
                name: 'Ombre star print crepe pleated empire dress',
                imageUrl: 'https://img1.eshakti.com/clothImages/CL0070655ML.jpg',
                price: 25,
                colors: ['red', 'orange', 'green', 'orangered'],
                sizes: ['XS', 'S', 'M', 'L', 'XL'],
                tag: 'Finests',
                rate: 3,
                gallery: [
                    'https://img1.eshakti.com/clothImages/CL0070655ML.jpg',
                    'https://img1.eshakti.com/clothImages/CL0070655ML1.jpg',
                    'https://img1.eshakti.com/clothImages/CL0070655ML2.jpg',
                    'https://img1.eshakti.com/clothImages/CL0070655ML3.jpg',
                    'https://img1.eshakti.com/clothImages/CL0070655ML4.jpg',
                ]
            },
            {
                id: 2,
                name: 'Pink Floral Leopard Print Cupped Maxi Dress',
                imageUrl: 'https://media.missguided.com/s/missguided/CL110615_set/1/pink-floral-leopard-print-cupped-maxi-dress',
                price: 18,
                colors: ['red', 'orange',],
                sizes: ['XS', 'S', 'M', 'L', 'XL'],
                tag: 'Finests',
                rate: 5,
                gallery: [
                    'https://media.missguided.com/s/missguided/CL110615_set/1/pink-floral-leopard-print-cupped-maxi-dress',
                    'https://media.missguided.com/i/missguided/CL110615_02?fmt=jpeg&fmt.jpeg.interlaced=true&$product-page__main--2x$',
                    'https://media.missguided.com/i/missguided/CL110615_03?fmt=jpeg&fmt.jpeg.interlaced=true&$product-page__main--2x$',
                    'https://media.missguided.com/i/missguided/CL110615_04?fmt=jpeg&fmt.jpeg.interlaced=true&$product-page__main--2x$',
                ]
            },
            {
                id: 3,
                name: 'Black Ribbed X Front Bandage Cami Midaxi Dress',
                imageUrl: 'https://media.missguided.com/s/missguided/DE930536_set/1/black-ribbed-x-front-bandage-cami-midaxi-dress',
                price: 35,
                colors: ['black', 'orange', 'green', 'orangered'],
                sizes: ['XS', 'S', 'M', 'L', 'XL'],
                tag: 'Fresh',
                rate: 4,
                gallery: [
                    'https://media.missguided.com/s/missguided/DE930536_set/1/black-ribbed-x-front-bandage-cami-midaxi-dress',
                    'https://media.missguided.com/i/missguided/DE930536_04?fmt=jpeg&fmt.jpeg.interlaced=true&$product-page__main--2x$',
                    'https://media.missguided.com/i/missguided/DE930536_03?fmt=jpeg&fmt.jpeg.interlaced=true&$product-page__main--2x$',
                    'https://media.missguided.com/i/missguided/DE930536_02?fmt=jpeg&fmt.jpeg.interlaced=true&$product-page__main--2x$',
                ]
            },
            {
                id: 4,
                name: 'Chi Chi Caris Dress',
                imageUrl: 'https://www.chichiclothing.com/media/catalog/product/3/2/3216bl-_2.jpg?quality=80&bg-color=255,255,255&fit=bounds&height=&width=',
                price: 25,
                colors: ['black', 'orange', 'green', 'orangered'],
                sizes: ['XS', 'S', 'M', 'L', 'XL'],
                tag: 'Fresh',
                rate: 4,
                gallery: [
                    'https://www.chichiclothing.com/media/catalog/product/3/2/3216bl-_2.jpg?quality=80&bg-color=255,255,255&fit=bounds&height=&width=',
                    'https://www.chichiclothing.com/media/catalog/product/3/2/3216bl-_14.jpg?quality=80&bg-color=255,255,255&fit=bounds&height=&width=',
                    'https://www.chichiclothing.com/media/catalog/product/3/2/3216bl-_26.jpg?quality=80&bg-color=255,255,255&fit=bounds&height=&width=',
                    'https://www.chichiclothing.com/media/catalog/product/3/2/3216bl-_8.jpg?quality=80&bg-color=255,255,255&fit=bounds&height=&width=',
                ]
            },
            {
                id: 5,
                name: 'Lace Patchwork Off the Shoulder Maxi Dress',
                imageUrl: 'https://www.rosewe.com/images/201905/source_img/222565_G_15592705794710.JPG',
                price: 18,
                colors: ['black', 'orange', 'green', 'orangered'],
                sizes: ['XS', 'S', 'M', 'L', 'XL'],
                tag: 'Fresh',
                rate: 4,
                gallery: [
                    'https://www.rosewe.com/images/201905/source_img/222565_G_15592705794710.JPG',
                    'https://www.rosewe.com/images/201905/goods_img/222565_G_15592705792720.jpg',
                    'http://www.liligal.com/images/201905/source_img/222565_P_15592705795711.JPG',
                    'https://i.pinimg.com/originals/7c/73/d7/7c73d7f18eaaeb9bc05679cb9c9fa1e9.jpg',
                    'http://www.liligal.com/images/201905/source_img/222565_P_15592705793134.JPG',

                ]
            },
            {
                id: 6,
                name: 'Pocket Design Black V Neck Half Sleeve Dress',
                imageUrl: 'https://www.rosewe.com/images/201704/source_img/178454_G_1493027182313.jpg',
                price: 14,
                colors: ['black', 'orange', 'green', 'orangered'],
                sizes: ['XS', 'S', 'M', 'L', 'XL'],
                tag: 'Fresh',
                rate: 4,
                gallery: [
                    'https://www.rosewe.com/images/201704/source_img/178454_P_1493027182736.jpg',
                    'https://www.rosewe.com/images/201704/source_img/178454_P_1493027182086.jpg',
                    'https://www.rosewe.com/images/201704/source_img/178454_P_1493027182439.jpg',

                ]
            },
            {
                id: 7,
                name: 'Emerald Green Criss Cross Back Frill Hem Bodycon Dress',
                imageUrl: 'https://cdn-img.prettylittlething.com/b/7/3/0/b7300c185a64f6bc479cc80627519fbced48acff_CLV6251_1.JPG',
                price: 18,
                colors: ['black', 'orange', 'green', 'orangered'],
                sizes: ['XS', 'S', 'M', 'L', 'XL'],
                tag: 'Fresh',
                rate: 4,
                gallery: [
                    'https://cdn-img.prettylittlething.com/b/7/3/0/b7300c185a64f6bc479cc80627519fbced48acff_CLV6251_1.JPG',
                    'https://cdn-img.prettylittlething.com/7/2/7/4/72748a00dddfeca0b09efa7e36756c3800907b01_CLV6251_2.JPG',
                    'https://cdn-img.prettylittlething.com/e/b/6/d/eb6dd2955ab6831db6f39205aca0720cb08f20bd_CLV6251_3.JPG',
                    'https://cdn-img.prettylittlething.com/b/1/4/a/b14aa4320bc03af56afd883e9b50bb1763bfc368_CLV6251_4.JPG',
                    'https://cdn-img.prettylittlething.com/d/b/9/d/db9d1f06d13b4a4ae3ef043dbe9cf0fdc71d495e_CLV6251_5.JPG',
                ]
            },
            {
                id: 8,
                name: 'Robe Verte Satinée À Imprimé Floral | Robes | PrettyLittleThing FR',
                imageUrl: 'https://cdn-img.prettylittlething.com/2/f/e/b/2feb20e4e1bf55b3f4d316df0e8085ddbf626344_CLU7901_1.JPG',
                price: 14,
                colors: ['black', 'orange', 'green', 'orangered'],
                sizes: ['XS', 'S', 'M', 'L', 'XL'],
                tag: 'Fresh',
                rate: 4,
                gallery: [
                    'https://cdn-img.prettylittlething.com/2/f/e/b/2feb20e4e1bf55b3f4d316df0e8085ddbf626344_CLU7901_1.JPG',
                    'https://cdn-img.prettylittlething.com/0/7/8/2/0782e05bcebb36d4e0bb1443cab92dade49d7c21_CLU7901_2.JPG',
                    'https://cdn-img.prettylittlething.com/f/a/2/a/fa2adae9ec927673e44e7f8e367f94d4edde6528_CLU7901_3.JPG',
                    'https://cdn-img.prettylittlething.com/c/1/c/9/c1c9dbb236f03267c7010dda5ba68550d3dfd793_CLU7901_4.JPG',
                    'https://cdn-img.prettylittlething.com/0/4/d/b/04dbbfda64be9a5df4928f8984ea4cca60ff4278_CLU7901_5.JPG',
                ]
            },
            {
                id: 9,
                name: 'LUNA DRESS black',
                imageUrl: 'https://cdn.fashionbunker.com/media/catalog/product/cache/3/image/1200x1800/9df78eab33525d08d6e5fb8d27136e95/f/k/fk_20190632_luna_dress_001-black_g_62705_2.jpg',
                price: 16,
                colors: ['black', 'orange', 'green', 'orangered'],
                sizes: ['XS', 'S', 'M', 'L', 'XL'],
                tag: 'Fresh',
                rate: 4,
                gallery: [
                    'https://cdn.shopify.com/s/files/1/0939/7336/products/fk_20190632_luna_dress_001-black_g_62705_1024x1024.jpg',
                    'https://cdn.shopify.com/s/files/1/0939/7336/products/fk_20190632_luna_dress_001-black_g_62736_1024x1024.jpg',
                    'https://cdn.shopify.com/s/files/1/0939/7336/products/fk_20190632_luna_dress_001-black_g_62714_1024x1024.jpg',
                    'https://cdn.shopify.com/s/files/1/0939/7336/products/fk_20190632_luna_dress_001-black_g_62737_1024x1024.jpg',
                ]
            }
        ]
    },
    tops: {
        id: 2,
        title: 'Tops',
        routeName: 'tops',
        items: [
            {
                id: 10,
                name: 'Top bandeau',
                imageUrl: 'https://static.bershka.net/4/photos2/2019/I/0/1/p/8898/256/800/8898256800_1_1_3.jpg?t=1574357021468',
                price: 220
            },
            {
                id: 11,
                name: 'Black Ribbed Sleeveless Crop Top',
                imageUrl: 'https://media.missguided.com/s/missguided/TJC11821_set/1/black-ribbed-sleeveless-crop-top',
                price: 280
            },
            {
                id: 12,
                name: 'Stassie x Missguided Top corsé de satén en champán',
                imageUrl: 'https://media.missguided.com/s/missguided/TW626959_set/1/stassie-x-missguided-top-cors-de-satn-en-champn',
                price: 110
            },
            {
                id: 13,
                name: 'Missguided white satin corset top Size 6, never worn',
                imageUrl: 'https://d2h1pu99sxkfvn.cloudfront.net/b0/850557/537103721_KjnILoB4vk/P0.jpg',
                price: 160
            },
            {
                id: 14,
                name: 'White Ruched Corset Top',
                imageUrl: 'https://media.missguided.com/s/missguided/TJF36509_set/1/white-ruched-corset-top',
                price: 160
            },
            {
                id: 15,
                name: 'Crop Top Tiras Cruzadas Frente -50%',
                imageUrl: 'https://www.petuniaropa.com/wp-content/uploads/2019/04/11Crop-Top-Tiras-Cruzadas-Frente.jpg',
                price: 160
            },
            {
                id: 16,
                name: 'Cami Bralette Crop Top RED WINE',
                imageUrl: 'https://gloimg.zafcdn.com/zaful/pdm-product-pic/Clothing/2018/06/25/goods-first-img/1541471480933670166.jpg',
                price: 190
            },
            {
                id: 17,
                name: 'Top Halter Morley Fluo',
                imageUrl: 'https://tiendaonda.com.ar/wp-content/uploads/2019/07/top-hakter-morley-fino_1.jpg',
                price: 200
            }
        ]
    },
    pants: {
        id: 3,
        title: 'Pants',
        routeName: 'pants',
        items: [
            {
                id: 18,
                name: 'The Lion King Luau High Waisted Pants for Women by Minkpink',
                imageUrl: 'https://cdn-ssl.s7.disneystore.com/is/image/DisneyShopping/3258059832101-1',
                price: 125
            },
            {
                id: 19,
                name: 'Solly Trousers & Leggings, Allen Solly Black Casual Pants for Women',
                imageUrl: 'https://allensolly.imgix.net/img/app/product/2/232026-809985.jpg',
                price: 90
            },
            {
                id: 20,
                name: 'Fashion Simple Vertical Stripe Printed Ruffled High Rise Capri Tapered',
                imageUrl: 'https://images.beautifulhalo.com/images/392x588/201905/O/fashion-simple-vertical-stripe-printed-ruffled-high-rise-capri-tapered-pants-for-women_1557425944961.jpg',
                price: 90
            },
            {
                id: 21,
                name: 'High-Waisted Sateen Rockstar Super Skinny Cargo Pants for Women',
                imageUrl: 'https://oldnavy.gap.com/webcontent/0017/768/095/cn17768095.jpg',
                price: 165
            },
            {
                id: 22,
                name: 'High-Waisted Sateen Rockstar Super Skinny Cargo Pants for Women',
                imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQuTFvXApymZXrrbvThyKNA6IXyONE6EG0XrWO-oQ4V2Zayabn&s',
                price: 185
            }
        ]
    },
    swimwear: {
        id: 4,
        title: 'Womens',
        routeName: 'womens',
        items: [
            {
                id: 23,
                name: 'Blue Tanktop',
                imageUrl: 'https://i.ibb.co/7CQVJNm/blue-tank.png',
                price: 25
            },
            {
                id: 24,
                name: 'Floral Blouse',
                imageUrl: 'https://i.ibb.co/4W2DGKm/floral-blouse.png',
                price: 20
            },
            {
                id: 25,
                name: 'Floral Dress',
                imageUrl: 'https://i.ibb.co/KV18Ysr/floral-skirt.png',
                price: 80
            },
            {
                id: 26,
                name: 'Red Dots Dress',
                imageUrl: 'https://i.ibb.co/N3BN1bh/red-polka-dot-dress.png',
                price: 80
            },
            {
                id: 27,
                name: 'Striped Sweater',
                imageUrl: 'https://i.ibb.co/KmSkMbH/striped-sweater.png',
                price: 45
            },
            {
                id: 28,
                name: 'Yellow Track Suit',
                imageUrl: 'https://i.ibb.co/v1cvwNf/yellow-track-suit.png',
                price: 135
            },
            {
                id: 29,
                name: 'White Blouse',
                imageUrl: 'https://i.ibb.co/qBcrsJg/white-vest.png',
                price: 20
            }
        ]
    },
    active: {
        id: 5,
        title: 'Mens',
        routeName: 'mens',
        items: [
            {
                id: 30,
                name: 'Camo Down Vest',
                imageUrl: 'https://i.ibb.co/xJS0T3Y/camo-vest.png',
                price: 325
            },
            {
                id: 31,
                name: 'Floral T-shirt',
                imageUrl: 'https://i.ibb.co/qMQ75QZ/floral-shirt.png',
                price: 20
            },
            {
                id: 32,
                name: 'Black & White Longsleeve',
                imageUrl: 'https://i.ibb.co/55z32tw/long-sleeve.png',
                price: 25
            },
            {
                id: 33,
                name: 'Pink T-shirt',
                imageUrl: 'https://i.ibb.co/RvwnBL8/pink-shirt.png',
                price: 25
            },
            {
                id: 34,
                name: 'Jean Long Sleeve',
                imageUrl: 'https://i.ibb.co/VpW4x5t/roll-up-jean-shirt.png',
                price: 40
            },
            {
                id: 35,
                name: 'Burgundy T-shirt',
                imageUrl: 'https://i.ibb.co/mh3VM1f/polka-dot-shirt.png',
                price: 25
            }
        ]
    }
};

export default SHOP_DATA;
