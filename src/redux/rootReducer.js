import { combineReducers } from "redux";
import { persistReducer } from "redux-persist"; //is used when you wanna persist some data localy
import storage from "redux-persist/lib/storage";
import userReducer from "./user/UserReducer";

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["cart"], //we will persist only cart data
};

const rootReducer = combineReducers({
  user: userReducer,
});

export default rootReducer;
// persistReducer(persistConfig, rootReducer);
