import {
    EMAIL_SIGN_IN_START,
    EMAIL_SIGN_IN_SUCCESS,
    EMAIL_SIGN_IN_FAILURE,
    GOOGLE_SIGN_IN_START,
    GOOGLE_SIGN_IN_SUCCESS,
    GOOGLE_SIGN_IN_FAILURE,
    SET_CURRENT_USER
} from './UserTypes';

export const setCurrentUser = (user) => ({ type: SET_CURRENT_USER, payload: user }); // the setCurrentUser accept the user that its going to be logged in the page

export const googleSignInStart = () => ({ type: GOOGLE_SIGN_IN_START }); //this event its no tracked by the reducer, its tracked by the saga
export const googleSignInSuccess = (user) => ({ type: GOOGLE_SIGN_IN_SUCCESS, payload: user });
export const googleSignInFailure = (error) => ({ type: GOOGLE_SIGN_IN_FAILURE, payload: error });

export const emailSignInStart = () => ({ type: EMAIL_SIGN_IN_START });
export const emailSignInSuccess = (user) => ({ type: EMAIL_SIGN_IN_SUCCESS, payload: user });
export const emailSignInFailure = (error) => ({ type: EMAIL_SIGN_IN_FAILURE, payload: error });

