//requirements to make the reducer are the types
//needs to have a initial state

import {
    SET_CURRENT_USER,
    GOOGLE_SIGN_IN_SUCCESS,
    EMAIL_SIGN_IN_SUCCESS,
    GOOGLE_SIGN_IN_FAILURE,
    EMAIL_SIGN_IN_FAILURE
} from './UserTypes';

const INITIAL_STATE = { currentUser: null, error: null };

//the reducer have two arguments the state and the action that its part of that state
//the action have a type and a optional payload

const userReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SET_CURRENT_USER:
            return { ...state, currentUser: action.payload };

        case GOOGLE_SIGN_IN_SUCCESS:
        case EMAIL_SIGN_IN_SUCCESS:
            return { ...state, currentUser: action.payload, error: null };

        case GOOGLE_SIGN_IN_FAILURE:
        case EMAIL_SIGN_IN_FAILURE:
            return { ...state, currentUser: action.payload };

        default:
            return state;
    }
}

export default userReducer;