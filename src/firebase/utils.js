import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyCrWFJV896orrcmoYpSVCnhVB_8VAS_Za8",
    authDomain: "ecommece-clothing-app.firebaseapp.com",
    databaseURL: "https://ecommece-clothing-app.firebaseio.com",
    projectId: "ecommece-clothing-app",
    storageBucket: "ecommece-clothing-app.appspot.com",
    messagingSenderId: "680847133955",
    appId: "1:680847133955:web:1cc9d6b3fabd66378bed36",
    measurementId: "G-SZXKN83WB7"
  };

  firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

export const googleProvider = new firebase.auth.GoogleAuthProvider();

export default firebase;