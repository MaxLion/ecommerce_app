import React from 'react'
import { Route } from 'react-router-dom';
import Collection from '../../components/Collection/Collection';
import CollectionItem from '../../components/CollectionItem/CollectionItem';
import BackWall from '../../components/BackWall/BackWall';

const Shop = ({ match }) => (
    <div className='shop-page'>
        <Route exact path={`${match.path}/:collectionId`} component={Collection} />
        <Route path={`${match.path}/:collectionId/:itemId`} component={CollectionItem} />
        <BackWall blur='0px' />
    </div>
);

export default Shop;