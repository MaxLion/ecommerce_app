import React from 'react';
import Directory from '../../components/Directory/Directory';
import BackWall from '../../components/BackWall/BackWall';
import styles from './home.module.scss';

function Home() {
    return (
        <div className={styles.home}>
            <Directory />
            <BackWall />
        </div>
    );
}

export default Home;