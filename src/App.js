import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import Home from './pages/home/Home';
import Shop from './pages/shop/Shop';
import Header from './components/Header/Header';
import SigninSignup from './pages/signin_signup/signin_signup';
import styles from './App.module.scss';

class App extends Component {
  render() {
    return (
      <Router>
        <Header />
        <div className={styles.container}>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/shop" component={Shop} />
            <Route exact path='/signin' render={() => this.props.currentUser ? (<Redirect to='/' />) : (<SigninSignup />)} />
          </Switch>
        </div>
      </Router>
    )
  }
}

export default App;
